#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# File Column Value Counter
#
# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import csv as im_csv

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

def is_n_to_show_progess( n ) :
	if n < 11:
		b = True
	elif n < 101 :
		b = (n % 10) == 0
	elif n < 1001 :
		b = (n % 100) == 0
	elif n < 10001 :
		b = (n % 1000) == 0
	elif n < 100001 :
		b = (n % 10000) == 0
	elif n < 1000001 :
		b = (n % 10000) == 0
	elif n < 10000001 :
		b = (n % 100000) == 0
	else :
		b = False
	return b

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

def Test_fileName():
	return "/home/ger/Downloads/following_accounts_twit_fedi.csv"

# --------------------------------------------------
# Processing
# --------------------------------------------------


def EnsureArrayHadDictionariesUpto( lst_CV , i_max ):
	# print( "EnsureArrayHadDictionariesUpto" + " : " + str( i_max) )
	for i in range( i_max) :
		if len( lst_CV) < i_max + 1 :
			lst_CV.append( {} )

def ToDictionary_AddValue_AndCount( d_d, i, v ):
	# print( "ToDictionary_AddValue_AndCount" )
	if not i in d_d :
		d_d[ i] = {}
	if v in d_d[ i] :
		d_d[ i][ v] += 1
	else :
		d_d[ i][ v] = 1

def ShowCurrentCounts( d_d ):
	print( "ShowCurrentCounts" )
	for k in d_d.keys() :
		print( "Column " + str( k) + " value count = " + str( len( d_d[ k]) ) )
		if len( d_d[ k]) < 15 :
			print( d_d[ k] )

def File_Column_Value_Counter( fn, do_exponential_showings ):
	print( "File_Column_Value_Counter" + " : " + fn)
	# setup
	giving_up = False
	max_ubnd = -1
	lcount = 0
	dct_Columns = {}
	# loop through the text file
	with open(fn) as f:
		csv_reader = im_csv.reader( f) # just relying on the default behaviour here
		for SplitString in csv_reader :
			lcount = lcount + 1
			ubnd = len( SplitString) - 1
			i = 0
			for v in SplitString :
				ToDictionary_AddValue_AndCount( dct_Columns, i, v )
				i += 1
			if do_exponential_showings and is_n_to_show_progess( lcount) :
				print( "--------- Showing at Line count: " + str( lcount ) )
				ShowCurrentCounts( dct_Columns )
			if giving_up:
				break
	print( "========= Showing at final Line count: " + str( lcount ) )
	ShowCurrentCounts( dct_Columns )

def Test_File_Column_Value_Counter():
	print( "Test_File_Column_Value_Counter BEGIN")
	File_Column_Value_Counter(Test_fileName(), False)
	print( "Test_File_Column_Value_Counter ENDED")

Test_File_Column_Value_Counter()
