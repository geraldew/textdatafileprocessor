#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# textdatafileprocessor = Text Data File Processor
#
# --------------------------------------------------
# Copyright (C) 2023-2023  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import csv as im_csv

import sys as im_sys
#import os as im_os
#import os.path as osp

import tkinter as im_tkntr 
import tkinter.ttk as im_tkntr_ttk
#import tkinter.scrolledtext as im_tkntr_tkst
from tkinter import scrolledtext as im_tkntr_tkst 
from tkinter import filedialog as im_filedialog 
from tkinter import messagebox as im_messagebox

import pathlib as im_pathlib

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# This needs to be here, not for time priority but for global scope
# global thismodule_log 

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "textdatafileprocessor"

def app_code_abbr():
	return "txdflprc"

def app_show_name():
	return "Text Data File Processor"

def app_show_tagline():
	return "A tool for processing and analysing text data files."

def app_code_abbr_s():
	return app_code_abbr() + " "

def app_version_code_show() :
	return "v0.0.3"

def app_show_name_version() :
	return app_show_name() + " " + app_version_code_show()

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

def is_n_to_show_progess( n ) :
	if n < 11:
		b = True
	elif n < 101 :
		b = (n % 10) == 0
	elif n < 1001 :
		b = (n % 100) == 0
	elif n < 10001 :
		b = (n % 1000) == 0
	elif n < 100001 :
		b = (n % 10000) == 0
	elif n < 1000001 :
		b = (n % 10000) == 0
	elif n < 10000001 :
		b = (n % 100000) == 0
	else :
		b = False
	return b

def tbracketed( s ):
	return '\u2772' + s + '\u2773'

# --------------------------------------------------
# Class extentions - tkinter
# --------------------------------------------------

class tkntr_VerticalScrolledFrame( im_tkntr.Frame):
	"""A pure Tkinter scrollable frame that actually works!
	* Use the 'interior' attribute to place widgets inside the scrollable frame
	* Construct and pack/place/grid normally
	* This frame only allows vertical scrolling
	"""
	def __init__(self, parent, *args, **kw):
		im_tkntr.Frame.__init__(self, parent, *args, **kw)
		# create a canvas object and a vertical scrollbar for scrolling it
		vscrollbar = im_tkntr.Scrollbar( self, orient=im_tkntr.VERTICAL)
		vscrollbar.pack( fill=im_tkntr.Y, side=im_tkntr.RIGHT, expand=im_tkntr.FALSE)
		canvas = im_tkntr.Canvas( self, bd=0, highlightthickness=0, yscrollcommand=vscrollbar.set)
		canvas.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=im_tkntr.TRUE)
		vscrollbar.config( command=canvas.yview)
		# reset the view
		canvas.xview_moveto(0)
		canvas.yview_moveto(0)
		# create a frame inside the canvas which will be scrolled with it
		self.interior = interior = im_tkntr.Frame(canvas)
		interior_id = canvas.create_window( 0, 0, window=interior, anchor=im_tkntr.NW)
		# track changes to the canvas and frame width and sync them,
		# also updating the scrollbar
		def _configure_interior(event):
			# update the scrollbars to match the size of the inner frame
			size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
			canvas.config( scrollregion="0 0 %s %s" % size)
			if interior.winfo_reqwidth() != canvas.winfo_width():
				# update the canvas's width to fit the inner frame
				# canvas.config( width=interior.winfo_reqwidth()) # height=interior.winfo_reqheight
				canvas.config( width=interior.winfo_reqwidth(), height=interior.winfo_reqheight() )
		interior.bind('<Configure>', _configure_interior)
		def _configure_canvas(event):
			if interior.winfo_reqwidth() != canvas.winfo_width():
				# update the inner frame's width to fill the canvas
				canvas.itemconfigure(interior_id, width=canvas.winfo_width())
		canvas.bind('<Configure>', _configure_canvas)

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

def Test_fileName():
	return "/home/ger/Downloads/following_accounts_twit_fedi.csv"

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Processing
# --------------------------------------------------

"""
The crux of this is to store and manage the findings a "Structure"
First level set of keys is the column index from 0 to n-1 columns:
	Second level set of keys is either:
	- h = header info
	- d = data info
	For the header, inside is a dictionary keyed on 
		each value being the column name string
	For the data, inside is a dictionary keyed on column index from 0 to n-1 columns
		each value being a dictionary of the discovered values in that column
			each value being the number of times that value has shown up
"""

def rk_head():
	return "h"

def rk_data():
	return "d"


def EnsureArrayHadDictionariesUpto( lst_CV , i_max ):
	# print( "EnsureArrayHadDictionariesUpto" + " : " + str( i_max) )
	for i in range( i_max) :
		if len( lst_CV) < i_max + 1 :
			lst_CV.append( {} )

def ToDictionary_AddValue_AndCount( r_dct, p_v ):
	# print( "ToDictionary_AddValue_AndCount" )
	#if not i in d_d :
	#	d_d[ i] = {}
	if not p_v in r_dct :
		r_dct[ p_v] = 1
	else :
		r_dct[ p_v] += 1

def ShowCurrentCounts( fn_show, d_d ):
	if fn_show is None:
		fn_show = print
	fn_show( "ShowCurrentCounts" )
	for k_0 in d_d.keys() :
		i_str = "Column " + str( k_0 )
		if rk_head() in d_d[ k_0]:
			i_str += " heading: " + d_d[ k_0][ rk_head() ]
		fn_show( i_str )
		if rk_data() in d_d[ k_0]:
			i_len = len( d_d[ k_0][ rk_data() ] )
			i_str += " value count: " + str( i_len )
			fn_show( i_str )
			if i_len < 21 :
				i_str = "values and frequencies: "
				for i_k in d_d[ k_0][ rk_data() ].keys() :
					i_str += i_k + " " + tbracketed( str( d_d[ k_0][ rk_data() ][ i_k ] ) )
				fn_show( i_str )

def File_Column_Value_Counter( fn ):
	#print( "File_Column_Value_Counter" + " : " + fn)
	# setup
	giving_up = False
	max_ubnd = -1
	lcount = 0
	dct_Columns = {}
	# loop through the text file
	with open(fn) as f:
		csv_reader = im_csv.reader( f) # , delimiter=' ', quotechar='|'
		for SplitString in csv_reader :
			lcount = lcount + 1
			#print( "  Split" ) 
			#print( SplitString ) 
			ubnd = len( SplitString) - 1
			i = 0
			for v in SplitString :
				ToDictionary_AddValue_AndCount( dct_Columns, i, v )
				i += 1
			if is_n_to_show_progess( lcount) :
				ShowCurrentCounts( print, dct_Columns )
			if giving_up:
				break
	print( "DONE File_Column_Value_Counter" + " Line count: " + str( lcount ) )
	ShowCurrentCounts( print, dct_Columns )

def Test_File_Column_Value_Counter():
	print( "Test_File_Column_Value_Counter")
	File_Column_Value_Counter(Test_fileName())

# Test_File_Column_Value_Counter()

def Interact_File_Column_Value_Counter( p_filnam, p_1st_is_header, r_Structure, p_fn_show_during, p_fn_show_result, p_fn_refresh ):
	# p_filnam = string file reference 
	# dct = the dictionary (of dictionaries) to be filled by the process, partly to make it clear it's not local to this function
	# p_fn_show_during = passing in a function that should be used to show findings during the processing run, presume it takes a string
	# p_fn_show_result = passing in a function that should be used to show findings after the processing run, presume it takes a string
	# p_fn_refresh = passing in a function that will trigger a refresh of the GUI display - i.e. crude, but will do for now
	p_fn_show_during( "Interact_File_Column_Value_Counter" + " : " + p_filnam)
	# setup
	if r_Structure is None :
		r_Structure = {}
	giving_up = False
	r_ok = True
	max_ubnd = -1
	lcount = 0
	# loop through the text file
	f = open( p_filnam, mode='r')
	csv_reader = im_csv.reader( f) # , delimiter=' ', quotechar='|'
	for SplitString in csv_reader :
		lcount = lcount + 1
		if False:
			#print( "  Split" ) 
			#print( SplitString ) 
			p_fn_show_during( "Line " + str( lcount ) + " has " + str( len( SplitString )) + " columns" )
		if lcount == 1 and p_1st_is_header :
			#print( "doing header")
			i_i = 0
			for i_str_h in SplitString :
				#print( "doing header column: " + i_str_h )
				r_Structure[ i_i ] = {}
				r_Structure[ i_i ][ rk_head() ] = i_str_h
				i_i += 1
		else :
			#print( "doing a data line")
			# ensure we have value dictionaries for any columns
			if len( SplitString ) > max_ubnd + 1 :
				#print( "ensuring dictionaries")
				for i_i in range( len( SplitString ) ) :
					#print( "ensuring column " + str( i_i) )
					# we have allow for maybe having already done the headers or maybe bot
					if i_i in r_Structure :
						if not rk_data() in r_Structure[ i_i ] :
							#print( "setting empty data dct for column " + str( i_i) )
							r_Structure[ i_i ][ rk_data() ] = {}
					else :
						#print( "setting meta dct for column " + str( i_i) )
						r_Structure[ i_i ] = {}
						#print( "setting empty data dct for column " + str( i_i) )
						r_Structure[ i_i ][ rk_data() ] = {}
				max_ubnd = len( SplitString ) - 1
				#print( "new max_ubnd: " + str( max_ubnd) )
			#print( "doing a data values")
			#print( "r_Structure" )
			#print( r_Structure )
			i_i = 0
			for v in SplitString :
				#print( "r_Structure[i_i]" + " for: " + str(i_i) )
				#print( r_Structure[i_i] )
				ToDictionary_AddValue_AndCount( r_Structure[i_i][ rk_data() ], v )
				i_i += 1
		if is_n_to_show_progess( lcount) :
			ShowCurrentCounts( p_fn_show_during, r_Structure )
		if giving_up:
			break
	f.close()
	print( "Finished file run." )
	p_fn_show_result( "DONE File_Column_Value_Counter" + " Line count: " + str( lcount ) )
	print( "Showing result data." )
	ShowCurrentCounts( p_fn_show_result, r_Structure )
	print( "Returning" )
	return "Ok", r_ok, r_Structure

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def getpathref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_foldref = im_filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return i_foldref

def getloadfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Load from"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def getsavefileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Save to"
	if len(from_folder_name) == 0 :
		from_folder_name = str( im_pathlib.Path.home() )
	root = im_tkntr.Tk()
	root.withdraw()
	i_fileref = im_filedialog.asksaveasfilename( title=show_title, initialdir=from_folder_name)
	return i_fileref

def ApplicationWindow( p_window_context ) : 
	# Brief:
	# Provide all the details of the user interface 
	# Parameters:
	# - p_window_context - a tkinter parent reference
	# Returns:
	# Notes:
	# the code structure used here is to first have all the sub "def"s for the element actions
	# then define all the elements
	# As there is zero likelihood of wanting multiple instances, it is NOT written as a Class
	# the concept being implemented is a single window with a notebook of multiple tabs
	# ------------
	# Code layout:
	# - Feature defs
	# - - all tabs
	# - - per tab
	# - GUI action defs
	# - - all tabs
	# - - per tab
	# - GUI definition block
	# - - create Notebook of tabs
	# - - per tab elements, usually as multiple frames
	# ------------
	# tkinter settings functions
	# tk metrics
	def tk_padx():
		return 9
	def tk_pady():
		return 3
	# Feature defs
	def bool_to_chkboxvar( p_bool):
		if p_bool :
			v = 1
		else :
			v = 0
		return v
	def colour_bg_danger():
		return "plum1"
	def colour_bg_safely():
		return "PaleGreen1"
	def colour_bg_config():
		return "orange"
	def not_running_else_say( str_action ) :
		# concept here is to return whether we're free to run a process and if not to show a message about it
		if process_running :
			# Choice between using showinfo, showwarning and showerror
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# ------------
	# GUI interaction defs
	# For Tab 1
	def on_button_exit() :
		# protect from exiting while a process is running for when is asynchronous
		if not_running_else_say( "Exiting" ) :
			p_window_context.quit()
	# For Tab 2
	def t2_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if tb2_lock_ctrls :
			im_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# File A
	def t2_on_button_browse_file_a() :
		if t2_not_running_else_say( "Browsing for File A" ) :
			got_path = t2_var_file_a.get()
			got_string = getloadfileref_fromuser( "Select File A", got_path)
			t2_var_file_a.set( got_string)
	# File B
	def t2_on_button_browse_file_b() :
		if t2_not_running_else_say( "Browsing for File B" ) :
			got_path = t2_var_file_b.get()
			got_string = getloadfileref_fromuser( "Select File B", got_path)
			t2_var_file_b.set( got_string)
	# File B
	def t2_on_button_browse_path_tmp() :
		if t2_not_running_else_say( "Browsing for Temp path" ) :
			got_path = t2_var_path_tmp.get()
			got_string = getpathref_fromuser( "Select Path", got_path)
			t2_var_path_tmp.set( got_string)
	def t2_on_button_action() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal process_running
		button_action_txt = "Run Action !"
		if process_running :
			if t2_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t2_label_actionise_status.configure( text = "Abandoned processing in mode:")
				process_running = False
				t2_button_actionise_text.set( button_action_txt )
		elif True :
			t2_label_actionise_status.configure( text = "Actioning..." + " mode:")
			process_running = True
			t2_button_actionise_text.set("Halt Action !")
			i_str_file_a = t2_var_file_a.get()
			i_1st_is_header = ( t2_header_cbv.get() == 1 )
			if ( len(i_str_file_a) > 0 ) :
				# action here
				str_outcome, i_ok, i_dct = Interact_File_Column_Value_Counter( i_str_file_a, i_1st_is_header, None, t5_ProcessLog_AddLine, t6_ResultLog_AddLine, None )
				if False:
					im_tkntr.messagebox.showwarning(app_show_name(), str_outcome) 
				if i_ok :
					t06_t4_populate_from_dct( i_dct)
			else :
				im_tkntr.messagebox.showwarning(app_show_name(), "File reference is empty!") 
			# return GUI to normal
			process_running = False
			t2_label_actionise_status.configure( text = "Run Terminated" )
			t2_button_actionise_text.set( button_action_txt )
		else :
			im_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a problem somewhere!") 
			t2_label_actionise_status.configure( text = "No Run. There is a clash somewhere!" )
	# For the Discovery Tab
	def t06_t4_populate_from_dct( p_dct):
		for i in p_dct.keys() :
			# frame for the line
			t06_t4_dct_frm[i] = im_tkntr.Frame( t06_t4_frame41.interior, bg=colour_bg_config())
			t06_t4_dct_frm[i].pack( fill = im_tkntr.X ) # , padx=tk_padx()
			# checkbox with the column index
			t06_t4_dct_cbv[i] = im_tkntr.IntVar()
			t06_t4_dct_cbx[i] = im_tkntr.Checkbutton(t06_t4_dct_frm[i], text=str(i), variable=t06_t4_dct_cbv[i], bg=colour_bg_config() )
			t06_t4_dct_cbx[i].pack( side=im_tkntr.LEFT, anchor=im_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # , padx=tk_padx() , expand=im_tkntr.YES
			# column names
			t_t = "Column #" + str( i + 1 )
			if rk_head() in p_dct[ i] :
				t_t += " " + p_dct[ i][ rk_head() ]
			t06_t4_labels[i] = im_tkntr.Label( t06_t4_dct_frm[i], text=t_t, bg=colour_bg_config())
			t06_t4_labels[i].pack( side = im_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
			# print( p_dct[i] ) 
			if rk_data() in p_dct[ i] :
				ss = str( len( p_dct[i][ rk_data() ] ) )
			t06_t4_counts[i] = im_tkntr.Label( t06_t4_dct_frm[i], text=tbracketed(ss), bg=colour_bg_config())
			t06_t4_counts[i].pack( side = im_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
			# combobox with the found values
			t06_t4_vals_str[i] = im_tkntr.StringVar() 
			t06_t4_vals_cmb[i] = im_tkntr_ttk.Combobox( t06_t4_dct_frm[i], state="readonly", textvariable = t06_t4_vals_str[i]) # , width = 40
			i_lst = []
			if rk_data() in p_dct[ i ] :
				for k in p_dct[ i ][ rk_data() ].keys():
					i_str = k + " " + tbracketed( str( p_dct[ i ][ rk_data() ][ k ] ) )
					i_lst.append( i_str )
			t06_t4_vals_cmb[i]['values'] = tuple( i_lst )
			t06_t4_vals_cmb[i].pack( side = im_tkntr.LEFT, padx=tk_padx(), pady=tk_pady(), expand=im_tkntr.YES, fill = im_tkntr.X ) # , expand=im_tkntr.YES
			# not currently doing anything with the counts per value
	# For Tab 5 GUI Process Log
	def t5_ProcessLog_AddLine( st ):
		st_plogs.insert(im_tkntr.END, '\n' + st)
		# print( st)
	def t6_ResultLog_AddLine( st ):
		st_rslts.insert(im_tkntr.END, '\n' + st)
		# print( st)
	# -------------------------------------------------
	# main for ApplicationWindow
	process_running = False
	# --------- Frame 0 Title
	# Have a top section outside the Tabbed interface
	frame_top = im_tkntr.Frame( p_window_context)
	frame_top.pack( fill = im_tkntr.X)
	# make frame content
	button_exit = im_tkntr.Button( frame_top, text="(X)", command=on_button_exit)
	label_title = im_tkntr.Label( frame_top, text=app_show_name_version() )
	label_explain = im_tkntr.Label( frame_top, text=app_show_tagline() )
	# place the content
	button_exit.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	label_title.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	label_explain.pack( side = im_tkntr.RIGHT, padx=5, pady=5)
	# Make the notebook i.e. tabbed interface
	nb = im_tkntr_ttk.Notebook( p_window_context)
	nb.pack( expand=1, fill=im_tkntr.BOTH)
	# ========== Make 1st tab = Reserved
	tab_1 = im_tkntr.Frame(nb)
	nb.add( tab_1, text="About")
	tb1_lock_ctrls = False
	# ========== Make and add the 2nd tab 
	tab_2 = im_tkntr.Frame(nb)
	nb.add( tab_2, text="Column Value Collation")
	tb2_lock_ctrls = False
	# --------- Frame 1 File A
	t2_frame1 = im_tkntr.Frame( tab_2, bg=colour_bg_safely())
	t2_frame1.pack( fill = im_tkntr.X)
	t2_label_browse_file_a = im_tkntr.Label( t2_frame1, text="File A", bg=colour_bg_safely())
	t2_label_browse_file_a.pack( side = im_tkntr.LEFT, padx=5, pady=5) 
	t2_var_file_a = im_tkntr.StringVar()
	t2_entry_file_a = im_tkntr.Entry( t2_frame1, textvariable=t2_var_file_a)
	t2_entry_file_a.pack( side = im_tkntr.LEFT, fill = im_tkntr.X, expand=1, padx=5, pady=5) 
	t2_button_browse_file_a = im_tkntr.Button( t2_frame1, text="Browse..", command=t2_on_button_browse_file_a)
	t2_button_browse_file_a.pack( side = im_tkntr.LEFT, padx=5, pady=5)
	# temp preset the filename
	t2_var_file_a.set( Test_fileName() )
	# --------- Frame 7 Controls
	t2_frame7 = im_tkntr.Frame( tab_2)
	t2_frame7.pack( fill = im_tkntr.X)
	# checkbox with the column index
	t2_header_cbv = im_tkntr.IntVar()
	t2_header_cbx = im_tkntr.Checkbutton( t2_frame7, text="First line is a header", variable=t2_header_cbv, bg=colour_bg_config() )
	t2_header_cbx.pack( side=im_tkntr.LEFT, anchor=im_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # , padx=tk_padx() , expand=im_tkntr.YES
	# --------- Frame 8 Action buttons
	t2_frame8 = im_tkntr.Frame( tab_2)
	t2_frame8.pack( fill = im_tkntr.X)
	#
	t2_button_actionise_text = im_tkntr.StringVar()
	t2_button_actionise_text.set("Analyse the file !")
	t2_button_actionise = im_tkntr.Button( t2_frame8, textvariable=t2_button_actionise_text, command=t2_on_button_action)
	t2_button_actionise.pack( side=im_tkntr.LEFT, padx=5, pady=5)
	t2_label_actionise_status = im_tkntr.Label( t2_frame8, text="_", fg = "blue") # , width=60
	t2_label_actionise_status.pack( side=im_tkntr.RIGHT )

	# --------- Frame 9 Discoveries
	t2_frame9 = im_tkntr.Frame( tab_2)
	t2_frame9.pack( fill = im_tkntr.X)
	#

	# --------- Frame 01 Disposal Match heading
	t06_t4_frame01 = im_tkntr.Frame( t2_frame9, bg=colour_bg_config())
	t06_t4_frame01.pack( fill = im_tkntr.X)
	t06_t4_label_heading_settings = im_tkntr.Label( t06_t4_frame01, text="Findings", bg=colour_bg_config())
	t06_t4_label_heading_settings.pack( side = im_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t4_label_settings_alert = im_tkntr.Label( t06_t4_frame01, text="_", fg = "blue", bg=colour_bg_config()) 
	t06_t4_label_settings_alert.pack( side = im_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 41 Items to have affected by a Save or a Load:
	# A place or list for each setting with a check box
	t06_t4_frame41 = tkntr_VerticalScrolledFrame( t2_frame9, bg=colour_bg_config())
	t06_t4_frame41.pack( fill = im_tkntr.X, expand=im_tkntr.YES)
	# prep the dictionaries for the multiple display elements
	t06_t4_dct_frm = {} # the Frames
	t06_t4_dct_cbv = {} # the values for the Checkboxes 
	t06_t4_dct_cbx = {} # the Checkboxes
	#
	t06_t4_labels = {} # the column labels
	t06_t4_counts = {} # the column labels
	#
	t06_t4_vals_str = {} # the selection variables for the Comboboxes
	t06_t4_vals_cmb = {} # the Comboboxes
	# ========== Make and add the 5th tab = Process Logs
	tab_5 = im_tkntr.Frame(nb)
	nb.add( tab_5, text="Process Logs")
	# ----------
	st_plogs = im_tkntr_tkst.ScrolledText( tab_5)
	st_plogs.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_plogs.insert(im_tkntr.INSERT, "GUI logging enabled.")
	#st_plogs.insert(im_tkntr.END, " in ScrolledText")
	# ========== Make and add the 6th tab = Results
	tab_6 = im_tkntr.Frame(nb)
	nb.add( tab_6, text="Results")
	# ----------
	st_rslts = im_tkntr_tkst.ScrolledText( tab_6)
	st_rslts.pack( side=im_tkntr.LEFT, fill=im_tkntr.BOTH, expand=True )
	st_rslts.insert(im_tkntr.INSERT, "No results yet!")

def main_gui_interact( ) :
	# Brief:
	# Enact the graphic user interface, allow the user set values and then launch actions.
	# Parameters:
	# Returns:
	# Notes:
	# this acts as a parent holder for the GUI as a window form with tkinter functions
	# Code:
	# setup the log controls
	# prep and launch the GUI
	root = im_tkntr.Tk()
	root.title(app_show_name())
	root.wm_geometry("780x500")
	# define the main window with tkinter features
	ApplicationWindow( root )
	# get the GUI in action
	root.mainloop()

def setup_logging() :
	#  this is currently just a placeholder for where the logging setup will be called
	return False

def command_line_actions( loadfilename ) :
	#  this is currently just a placeholder for where the command line action will be coded
	print( "command_line_actions")

def handle_command_line( lst_arg ) :
	# this is currently just framework code for deciding whether we're doing a command line or GUI
	# all that matters here is that 
	# gui_mode gets returned as True until we know what command line functions we want to have
	# at which point we'll come back to here and do clever stuff to enable that
	if __debug__:
		pass
	gui_mode = False
	loadfilename = ""
	if __debug__:
		pass
	for arg in lst_arg :
		if __debug__:
			pass
	some_action = False
	if not some_action :
		gui_mode = True
	if some_action and len(loadfilename) > 0 :
		if __debug__:
			pass
		command_line_actions( loadfilename,  ) 
		pass
	return gui_mode, loadfilename

# main to execute only if run as a program
if __name__ == "__main__":
	if __debug__:
		pass
	# command line parameter checking
	# later do something smarter
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# quite a few things to consider: e.g. allow pass parameters as presetting but then use the GUI
	# currently that logic is all in the "old" main_command_line which is lying fallow while the
	# GUI approach is being worked out in full, to in turn discover all the desired features
	arg_count = len(im_sys.argv)
	if arg_count < 2 :
		# in effect no parameters as the first/zeroth parameter is the command itself
		gui_mode = True
		preloadfilename = ""
	else:
		if __debug__:
			pass
		gui_mode, preloadfilename = handle_command_line( im_sys.argv ) 
	if gui_mode :
		if __debug__:
			pass
		main_gui_interact( ) # later make pass preloadfilename through for pickup into the GUI mode
	if __debug__:
		pass
