# textdatafileprocessor

A  Text Data File Processor written in Python.

Note: it therefore relies on having Python installed on a computer.

This is the repository overview. See the wiki (when it comes to exist) for more general information.

## Current status

Current status: It is now in development from proof of concept to a functioning application.

As an experiment, it will be shared via GitLab from its very early stages, well before being usable.

## Why write this?

The intention is to make a general purpose text data processing tool for doing things that are bothersome to enact as a custom action by other means.

It was prompted by a work situation where I had a very large text file of data that I wanted to get some basic analysis of, before importing into specific data tools.

While that situation led me to conceive of a specific feature, as there are likely to be other desired features, the name of the program will begin as something more generic.

It is being written for use on Ubuntu 20.04 Linux but hopefully might work on other platforms.

## Brief

This is to be a demonstraation program providing usable features for processing text data files of any size.

Thus while Python is not a good tool for writing high throughput data processing, this program can act as a prompt for someone else to create something equivalent with a better implementation.   

## Software License

This is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:
- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- Yes, I do know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- FOSS means you are free to study the code and use what you learn to write your own software, where you can thereby choose a license. No need to ask, please go ahead and do that (but see below about naming).

## Installation

No advice yet.

## Roadmap

No roadmap as such yet, just a mental conception of how its first feature might operate.

That feature is:
- a file column value counter - which will read through a text file of data, interpret each line as a row of data in columns, and as it reads through, compile lists of the values encountered per column

While the proof of concept can report on what it has found at intervals (and at complettion of reading), the intention is to have a GUI which will:
- provide an interactive selection of the file to process
- have a dynamic display of what has been discovered that changes with each new discovery

Quite how that "dynamic display" will look and work is the topic of ongoing development.

Similarly, quite what discovey saving/export options should be is yet to be determined - but does seem an obvious thing to add.

A clear problem is how to handle discovery of very large numbers of distinct values in the data.

It might be desirable to allow interaction with the discovery process - such that it could be pause, and from the values found, some addtional processing rules set, such that the ongiing collection remains manageable.

For example, if a column appears to have a sequence of steadily increasing numbers, then a rule could be to only note what the initial value found was, and to then keep the various difference values. e.g. Began_With = 12345, Delta_Increments = [1] (i.e. a list of only the numner 1, but ready to add to the list any other size jump in value between rows). 

- initial value
- final value
- minimum value
- maximum value
- average value - being calculable as current_averge * row_count + new_value / row_count + 1

## Implementation Details

Currently there are just two Python files: 
- `file_column_value_counter.py`
- `main_tkinter_application.py`

The file `file_column_value_counter.py` is a simple proof of concept which uses a hard-coded string for the file reference, and:
- uses a dictionary of dictionaries for in-memory acconting of what it finds
- uses a 1, 10, 100 etc rule for showing what it has found during the run
- also shows what it has found at the end of the run

The file `main_tkinter_application.py` will be where the GUI application will be written. It begins as a clone of a stock Tkinter single-window-with-tabs pro forma that I have.

## Implementation Plans

The challenge is going to be:
- how to visually present the collected and collated information - i.e. that's in memory variables
- how to update that information and have updates come through to the visual presentation
- how to allow the GUI to pause and resume the processing - somewhat presuming/expecting that there will be a thread method in use

### Visual ideas

- a row per data column
- column name/number, a count of values, a combobox of the values, the min value, the max value, etc

This should be feasible with the same techniques that I used in Foldatry for the Settings values to be saved/loaded

